#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <iostream>
#include <string>
#include <string.h>
#include <regex>
#include <netinet/in.h>
#include <netdb.h> 

using namespace std;

constexpr auto BUFF_SIZE = 1025;
char buff[BUFF_SIZE];

char* WebConnectCommunication(string url, string message);
string GetIpInBuffer(char* buff);

int main(int argc, char** argv)
{
	string message = "GET / HTTP/1.1\r\n User-Agent: Mozilla/4.0\r\n content-type:text/html\r\n \
							Connection: close\r\n\r\n";
	char* response = WebConnectCommunication("checkip.dyn.com", message);
	string ip = GetIpInBuffer(response);
	memset(&buff, 0, BUFF_SIZE);
	const string url = "kphk42.dothome.co.kr";
	sprintf(buff, "GET /IpChecking/UpdateIp.php?num=%s&ip='%s' HTTP/1.1\r\nHost: %s\r\nConnection: close\r\n\r\n", argv[1], ip.c_str(), url.c_str());
	message = buff;
	cout << message << endl;
	WebConnectCommunication(url, message);
}

char* WebConnectCommunication(string url, string message)
{
	const uint16_t port = 80;
	struct sockaddr_in server_addr;
	struct hostent *host;

	int client_socket = socket(PF_INET, SOCK_STREAM, 0);
	if (client_socket < 0)
	{
		cout << "socket 생성 실패" << endl;
		exit(1);
	}
	host = gethostbyname(url.c_str());

	memset(&server_addr, 0, sizeof(server_addr));

	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(port);
	server_addr.sin_addr.s_addr = *((unsigned long*)host->h_addr);

	if (connect(client_socket, (struct sockaddr*)&server_addr, sizeof(server_addr)) < 0)
	{
		cout << "접속 실패" << endl;
		exit(1);
	}

	if (send(client_socket, message.c_str(), message.length(), 0) < 0)
	{
		cout << "Error with send()" << endl;
	}
	else
	{
		cout << "Successfully sent html fetch request" << endl;
		if (read(client_socket, &buff, BUFF_SIZE) == 0)
		{
			cout << "웹 소스 구하기 실패" << endl;
		}
		else
		{
			cout << buff << endl;
		}
	}
	close(client_socket);
	return buff;
}

string GetIpInBuffer(char* buff)
{
	regex pattenIp("[0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+");
	smatch sm;
	string test = buff;
	regex_search(test, sm, pattenIp);
	return sm[0].str();
}